import {
    crerCompte,
    ajouterOperation,
    operationsSortir
} from "./components/compte";

const formAjoutCompte = document.getElementById("") as HTMLFormElement;
const formAjoutOperation = document.getElementById("") as HTMLFormElement;

const btnAjoutCompte = document.getElementById("") as HTMLButtonElement;
const btnAjoutOperation = document.getElementById("") as HTMLButtonElement;

btnAjoutCompte.addEventListener('click', crerCompte( formAjoutCompte ) );
btnAjoutOperation.addEventListener('click', ajouterOperation( formAjoutOperation ));

