import {Categories} from "../categories/Categories";

export class Operation {
    private id: number = Date.now();

    private type: string;

    private date: Date = new Date();

    private montant: number;

    private titre: string;

    private categorie: Categories;

    constructor(type: string, montant: number, titre: string, categorie: Categories) {
        this.type = type;
        this.montant = montant;
        this.titre = titre;
        this.categorie = categorie;
    }
}