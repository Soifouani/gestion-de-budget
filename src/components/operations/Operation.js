"use strict";
exports.__esModule = true;
exports.Operation = void 0;
var Operation = /** @class */ (function () {
    function Operation(type, montant, titre, categorie) {
        this.id = Date.now();
        this.date = new Date();
        this.type = type;
        this.montant = montant;
        this.titre = titre;
        this.categorie = categorie;
    }
    return Operation;
}());
exports.Operation = Operation;
