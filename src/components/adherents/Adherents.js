"use strict";
exports.__esModule = true;
exports.Adherents = void 0;
var Adherents = /** @class */ (function () {
    function Adherents(nom, prenom) {
        this.id = Date.now();
        this.nom = nom;
        this.prenom = prenom;
    }
    Adherents.prototype.ajouter = function () {
    };
    Adherents.prototype.supprimer = function () {
    };
    Adherents.prototype.modifier = function () {
    };
    Adherents.prototype.lister = function () {
        return null;
    };
    Adherents.prototype.recuperer = function () {
        return null;
    };
    return Adherents;
}());
exports.Adherents = Adherents;
