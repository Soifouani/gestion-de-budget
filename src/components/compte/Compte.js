"use strict";
exports.__esModule = true;
exports.Compte = void 0;
var Compte = /** @class */ (function () {
    function Compte(adherent) {
        this.id = Date.now();
        this.solde = 0;
        this.operations = [];
        this.adherent = adherent;
    }
    Compte.prototype.ajouter = function (comptes) {
        localStorage.setItem('comptes', JSON.stringify(comptes));
    };
    Compte.prototype.recuperer = function () {
        return null;
    };
    Compte.prototype.modifier = function () {
    };
    Compte.prototype.suprimer = function () {
    };
    Compte.prototype.lister = function () {
        var data = localStorage.getItem('comptes');
        if (data) {
            return JSON.parse(data);
        }
        return [];
    };
    Compte.prototype.entrer = function () {
    };
    Compte.prototype.sortir = function () {
    };
    return Compte;
}());
exports.Compte = Compte;
