import {Operation} from "../operations/Operation";
import {Adherents} from "../adherents/Adherents";

export class Compte {
    private id: number = Date.now();

    private solde: number = 0;
    
    private operations: Operation[] = [];

    private adherent: Adherents;

    constructor(adherent: Adherents) {
        this.adherent = adherent;
    }

    public get Id():number {
        return this.Id;
    }

    public set Id(id: number):void {
        this.Id = id;
    }

    ajouter(comptes: Compte[]): void {
        localStorage.setItem('comptes', JSON.stringify(comptes));
    }

    recuperer(): Compte {
        return null;
    }

    modifier(): void {

    }

    suprimer(): void {

    }

    lister(): Compte[] {
        const data: string = localStorage.getItem('comptes');
        if (data) {
           return JSON.parse(data);
        }
        return [];
    }

    entrer(operation: Operation): void {

    }

    sortir(operation: Operation): void {

    }
}