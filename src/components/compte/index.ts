import {Compte} from "./Compte";
import {Adherents} from "../adherents/Adherents";
import { Operation } from "../operations/Operation";
import { Categories } from "../categories/Categories";

let categorie = new Categories("Loisir")
let adherent= new Adherents("HAMADA", "Soifouani");
let operation = new Operation("entrer", 50, "Achat d'une gitard", categorie);
let compte = new Compte(adherent);

let comptes: Compte[] = compte.lister();

export function ajouter(formAjoutCompte: HTMLFormElement): void {

    const formData = new FormData( formAjoutCompte );
    formData.forEach((value, key) => compte[key] = value);

    comptes.push(compte);
    compte.ajouter(comptes);
}

export function ajouterOperation( formAjoutOperation: HTMLFormElement  ): void {
    let indexCompte: number = comptes.findIndex(compte => compte.Id === dataSetId );
    comptes.splice(indexCompte, 1);
    compte.entrer(operation);
}

export function operationsSortir(): void {
    compte.sortir(operation);
}